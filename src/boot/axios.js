import Vue from 'vue'
import axios from 'axios'

const apiDev = 'http://localhost/api/showcase'
const apiProd = 'https://vandecode.com/api/showcase'

const axiosInstance = axios.create({
  baseURL: (process.env.DEV) ? apiDev : apiProd,
  timeout: 60000,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
})
axiosInstance.interceptors.request.use(request => {
  return request
}, function (error) {
  return Promise.reject(error)
})
export default ({ store, vue }) => {
  Vue.prototype.$axios = axiosInstance
}
