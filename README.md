# Vue & Quasar Framework Showcase

## Demo
https://vandecode.com/showcase

## Environment
Frontend: Vue JS and Quasar<br/>
Backend: PHP native

## API
URL: https://vandecode.com/api/showcase<br/>
Endpoint: /book<br/>
Method: GET<br/>
Parameter: keyword<br/><br/>

Example single keyword: https://vandecode.com/api/showcase/book?keyword=cat<br/>
Example multiple keywords: https://vandecode.com/api/showcase/book?keyword=cat,dog

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```