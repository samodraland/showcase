<?php
    session_start();
    //error_reporting(0);

    header("Access-Control-Allow-Origin: *", false);
    header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0", false);
    header('Access-Control-Allow-Headers: token, Content-Type');
    header("Pragma: no-cache");
    header("Content-Type: application/json");

    $classLocation = "classes/";
    set_include_path ( "lib/" . PATH_SEPARATOR . $classLocation );
    spl_autoload_register();

    $param = $_GET['resource'];
    $jsonpcallback = $_GET['jsonpcallback'];
    $resource = explode("/",$param);
    $utils = new Utils();
    $method = $_SERVER['REQUEST_METHOD'];

    if( isset($param) && !empty($param) && !empty($resource[0]) && ctype_alnum(trim($resource[0])) ) {
        if (file_exists($classLocation.strtolower($resource[0]).".php")){
            $data = array();
            switch( $method ){
                case "GET":
                case "DELETE":
                    $data = $_GET;
                break;
                case "PUT":
                case "PATCH":
                    $inputs = file_get_contents("php://input");
                    parse_str($inputs,$post);
                    $data = array_merge($_GET,$post);
                break;
                default:
                    $data = array_merge($_FILES,$_POST,$_GET);
                break;
            }
            $checkclass = ucfirst($resource[0]);
            $class = new $checkclass;
            $result = call_user_func($class->doRequest($method), $data);
            header( $utils->getResponse( $result["response"], false ) );
            echo ( !$utils->hasValue($jsonpcallback) ) ? json_encode($result) : "$jsonpcallback(".json_encode($result).")";
        }else{
            header( $utils->getResponse(404) );
            echo json_encode( $utils->genericResponse(404) );
        }        
    }else{
        header( $utils->getResponse(404) );
        echo json_encode( $utils->genericResponse(404) );
    }
?>