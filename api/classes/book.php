<?php
    class Book{

        public function doRequest( $method ){
            $requests = array(
                "GET" => function( $param ){
                    return $this->getBook( $param );
                },
                "ERROR" => function( $param ){
                    return Utils::genericResponse(400);
                }
            );
            return ( array_key_exists($method, $requests) ) ? $requests[ $method ] : $requests[ "ERROR" ];
        }

        public function getBook( $param ){
            $keyword = trim(trim(strtolower($param["keyword"])),",");
            if (strpos($keyword,",")) {
                $arr = array_map("trim",explode(",",$keyword));
                $keyword = join("|",$arr);
            }
            $result = array();
            if ($keyword != ""){
                if (!isset($_SESSION["booklist"])) {
                    $_SESSION["booklist"] = json_decode(file_get_contents('static/Booklist.json'), true);
                }
            
                $result = array_values(array_filter($_SESSION['booklist'], function($item) use($keyword){                    
                    return $this->matchString($item["author"], $keyword)  === 1 || 
                    $this->matchString($item["series"], $keyword)  === 1 ||
                    $this->matchString($item["name"], $keyword) === 1;
                }));
            }

            return array(
                "response" => 200,
                "message" => Utils::getResponse(200),
                "result" => $result
            );
        }

        private function matchString($haystack, $needle){
            return preg_match("/\b(".strtolower(trim($needle)).")\b/",strtolower($haystack));
        }
    }
?>